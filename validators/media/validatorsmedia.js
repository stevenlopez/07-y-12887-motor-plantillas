const Joi = require('joi');

const mediaSchema = Joi.object({
    integranteId: Joi.number()
        .integer()
        .positive()
        .required()
        .messages({
            'number.base': 'El ID del integrante debe ser un número.',
            'number.integer': 'El ID del integrante debe ser un número entero.',
            'number.positive': 'El ID del integrante debe ser un número positivo.',
            'any.required': 'El ID del integrante es obligatorio.'
        }),
    tiposmediaId: Joi.string()
        .required()
        .messages({
            'any.required': 'El ID del tipo de media es obligatorio.'
        }),
    url: Joi.string()
        .uri()
        .allow('')
        .messages({
            'string.uri': 'La URL debe ser una dirección válida.'
        }),
    nombrearchivo: Joi.string()
        .allow('')
        .messages({
            'string.base': 'El nombre del archivo debe ser una cadena de caracteres.'
        }),
    activo: Joi.boolean()
        .required()
        .messages({
            'boolean.base': 'El estado activo debe ser un valor booleano.',
            'any.required': 'El estado activo es obligatorio.'
        }),
    video: Joi.binary()
        .max(1024 * 1024) // 1 MB en bytes
        .required()
        .messages({
            'any.required': 'Debe proporcionar un archivo de video.',
            'binary.max': 'El tamaño máximo permitido es 1 MB.'
        })
}).unknown();

module.exports = mediaSchema;